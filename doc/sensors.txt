
Sensors used in Cogito 
-=-=-=-=-=-=-=-=-=-=-=-

Smoke Sensor:
	Still unidentified. Hoping that it is one of the MQ's
	Assuming this might be useful: http://playground.arduino.cc/Main/MQGasSensors#wiring

Temperature Sensor:
	TMP36
	Has three pins. Ground, 3.3 or 5V and an analog output.
	Possible that more than one will be used, indoor/outdoor.

AC Current Sensor:
	Non invasiv current sensor.
	SCT-013-000
	100Amps
	0-100A = 0-50mV
	See http://openenergymonitor.org/emon/buildingblocks/ct-sensors-interface
	Also http://openenergymonitor.org/emon/buildingblocks/how-to-build-an-arduino-energy-monitor-measuring-current-only?page=1
	And http://www.arduinothaishop.com/productdetail.html?id=9097

LDR:
	For reading dark/light times of day, and possibly a secondary LDR to determine power saving (closed cupboard).
	Reading is simple with a voltage divider and properly calculated resistors.
	http://spacetinkerer.blogspot.com/2011/03/using-ldr-light-dependent-resistor-with.html

Water Flow:
	Complex, no sensor ordered yet.
	Need to determine where to connect it to main supply. Not easy.
	Coming back to this one later.

Humidity:
	For now, a single indoors humidity sensor.
	Need to order/source this.


