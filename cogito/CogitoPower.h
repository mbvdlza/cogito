#include "EmonLib.h"

#ifndef CogitoPower_H
#define CogitoPower_H

/*
  A wrapper for EmonLib. Abstracts most of the variables and finer details.
*/
class CogitoPower
{
  
  public:
    
    /* Calculate and return the immediate Current(rms) value and return a double */
    double getIrms();
    
    double getPower();
    
    /* Calculate and return an average of n samples of reading of Current(rms) and return a double */
    double getIrmsAvg();
    
    /* The last Current(rms) value calculated by EmonLib, as a double */
    double lastIrms;
    
    /* Constructor takes the input pin that the CT sensor circuit is connected to */
    void begin(int inputPin);
      
  private:
    EnergyMonitor emon;
    double emonICalibration;
    int currentSensorPin; 
};

#endif

