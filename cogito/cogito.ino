#include <EmonLib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "cogito.h"
#include "jobs.h"

/* PIN Definitions */
#define P_COGITO_POWER_INPUT 5

/* Timer Constants & Variables */
const char COUNTER_1S = 1;
const char COUNTER_2S = 2;
const char COUNTER_5S = 5;
const char COUNTER_1M = 60;
const int COUNTER_5M = 300;
const int COUNTER_15M = 900;

volatile uint16_t timercount;
int updateTimers = 0;


SensorDataPack sdp; 
CogitoPower power;


void setup()
{
  Serial.begin(19200);
  wdt_enable(WDTO_8S);  // watchdog threshold to 8seconds
  configureTimers();
  
  power.begin(P_COGITO_POWER_INPUT);
}

/*
  Main loop. No delay() or blocking code allowed.
*/
void loop()
{
  wdt_reset();  // watchdog reset
  if (updateTimers != 0)
  {
    timercount++;
    checkScheduler();
    updateTimers = 0;
  }
}

/* Publish whatever data is !!non-zero!! in the SensorDataPack to the network */
void publishData() 
{
  Serial.print("sdp.currentRms: ");
  Serial.println(sdp.currentRms);
  Serial.println("Pretending to make a network call here... pushing non zero data up.");
}

/*
  Configure the timer/s and interrupts.
 */
void configureTimers()
{
  cli();                  // disable global interrupts
  TCCR1A = TCCR1B = 0;    // set entire TCCR1x registers to 0

  OCR1A = 15624;          // set interrupt to desired timer value
  // 16 000 000 cpu clock / (1024 prescaler * 1 hz desired interrupt frequency) - 1

  TIMSK1 |= (1 << OCIE1A);  // set the OCIE1A bit to enable timer compare interrupt

  TCCR1B |= (1 << WGM12);   // set WGM12 bit to enable CTC mode

  TCCR1B |= (1 << CS10);    // setting the CS10 bit on the TCCR1B register, the timer is running.
  // ISR(TIMER1_OVF_vect) will be called when it overflows.
  // ISR(TIMER1_COMPA_vect) will be called when it compare matches.
  TCCR1B |= (1 << CS12);    // Setting CS10 and CS12 bits = clock/1024 prescaling

  sei();                    // enable global interrupts
}

/* The handler for timer1 compare match interrupt */
ISR(TIMER1_COMPA_vect)
{
  updateTimers = 1;
}

void checkScheduler()
{
  if ((timercount % COUNTER_2S) == 0) task_2S();
  if ((timercount % COUNTER_5S) == 0) task_5S();
  if ((timercount % COUNTER_1M) == 0) 
  {
    task_1M();
    publishData();  // minutely pushes of non zero data
  }
  if ((timercount % COUNTER_5M) == 0) task_5M();
  if ((timercount % COUNTER_15M) == 0) task_15M();     
}

