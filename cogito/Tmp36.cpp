#include "Tmp36.h"
#include "Arduino.h"

void Tmp36::begin(int sensorPin)
{
  this->tmp36pin = sensorPin;
}

float Tmp36::getSensorVoltage()
{
	/* 1024 possible values over 5 volt, 1024/5 = 205 per volt */
  float volts = analogRead(this->tmp36pin) / 205.0;
  return volts;
}

float Tmp36::getCelsius()
{
  return (this->getSensorVoltage() - 0.5) * 100.0;
}

float Tmp36::getFahrenheit()
{
  return (this->getCelsius() * 9.0 / 5.0) + 32.0;
}
