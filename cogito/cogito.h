#include "CogitoPower.h"
#include <queue>

#ifndef Cogito_H
#define Cogito_H

struct SensorDataPack {
  double currentRms;
  double currentRmsAvg;
  double power;
  
  SensorDataPack() : currentRms(0.0), power(0.0), currentRmsAvg(0.0) {}  // default constructor, all values zero
};

extern CogitoPower power;
extern SensorDataPack sdp; 

#endif Cogito_H
