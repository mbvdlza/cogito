#include "Arduino.h"
#include "jobs.h"
#include "cogito.h"

/* ============ TASKS ============ */
/* Keep code minimal, call out.    */




// Every 2 Seconds
void task_2S()
{
  Serial.println("2S fired");
}

// Every 5 Seconds
void task_5S()
{
  Serial.println("5S fired");
  sdp.currentRms = power.getIrmsAvg();
  sdp.power = power.getPower();
  sdp.currentRmsAvg = power.getIrmsAvg();
}

// Every 1 Minutes
void task_1M()
{
  Serial.println("1M fired");
}

// Every 5 Minutes
void task_5M()
{
  Serial.println("5M fired");
}

// Every 15 Minutes
void task_15M()
{
  Serial.println("15M fired");
}

