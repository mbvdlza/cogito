#include "Arduino.h"

#ifndef Tmp36_H
#define Tmp36_H

class Tmp36
{
  public:
    void begin(int sensorPin);
    float getCelsius();
    float getFahrenheit();

  private:
    int tmp36pin;
    float getSensorVoltage();
};


#endif
