#include "CogitoPower.h"
#include "EmonLib.h"

const double EMON_ICAL = 60.0;
const int EMON_NUMSAMPLES = 1480;
const int VOLTAGE_MAIN = 233.0;
const int AVG_SAMPLES = 5;

void CogitoPower::begin(int currentInputPin) 
{
  this->currentSensorPin = currentInputPin;
  this->emonICalibration = EMON_ICAL;
  
  this->emon.current(this->currentSensorPin, this->emonICalibration);
}

double CogitoPower::getIrms() 
{
  this->lastIrms = this->emon.calcIrms(EMON_NUMSAMPLES);
  return this->lastIrms; 
}

double CogitoPower::getPower()
{
  double current = this->emon.calcIrms(EMON_NUMSAMPLES);
  return current * VOLTAGE_MAIN;
}

double CogitoPower::getIrmsAvg()
{
  int n = 0;
  double avgCurrentTotal = 0;
  for (n = 0; n <= AVG_SAMPLES; n++)
  {
    avgCurrentTotal = avgCurrentTotal +  this->getIrms();
    delay(5);
  }
  return avgCurrentTotal / AVG_SAMPLES;
}
